package ir.execptionaldev.presentapp.repository.local

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import ir.execptionaldev.presentapp.repository.models.Get.User

@Dao
interface UserDao {

    @get:Query("SELECT * FROM users")
    val all: Single<List<User>>

    @get:Query("SELECT * FROM users LIMIT 1")
    val first: Observable<User>

    @Insert
    fun insertAll(users: List<User>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Update
    fun update(user: User): Single<Int>

    @Delete
    fun delete(user: User): Completable

    @Query("DELETE FROM users")
    fun deleteAll(): Completable

}