package ir.execptionaldev.presentapp.repository.models

import androidx.room.Entity
import androidx.room.PrimaryKey

class Get {
    data class Message(
            val code: String,
            val message: String
    )

    data class Status(
            val status: List<Message>
    )

    @Entity(tableName = "users")
    data class User(@PrimaryKey()
                    val id_user: Int,
                    val firstname: String,
                    val lastname: String,
                    val username: String?,
                    val email: String?,
                    val rank: String?,
                    val entrance_time: String?,
                    val exit_time: String?,
                    val id_present: String?,
                    val balance: Float?) {
        override fun toString(): String {
            return lastname
        }
    }

    data class UserList(
            val user_list: ArrayList<User>
    )
}