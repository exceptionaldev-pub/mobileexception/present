package ir.execptionaldev.presentapp.repository.models

class Send {
    data class AddPayment(
            val function: String,
            val id_user: String,
            val money: Float,
            val descr: String,
            val date: String
    )

    data class AddTimeData(
            val function: String,
            val id_user: String,
            val date: String,
            val time: String?,
            val descr: String
    )

    data class EditProf(
            val function: String,
            val id_user: String,
            val firstname: String,
            val lastname: String,
            val username: String,
            val password: String,
            val email: String
    )

    data class GetUsersData(
            val function: String
    )

    data class LoginData(
            val function: String,
            val username: String,
            val password: String
    )

    data class PresentHistoryData(
            val function: String,
            val date: String
    )

    data class PresentListData(
            val function: String,
            val day: String,
            val time: String
    )

    data class RegisterData(
            val function: String,
            val firstname: String,
            val lastname: String,
            val username: String,
            val password: String,
            val rank: String
    )

    data class EditPresent(
            val function: String,
            val id_present: String?,
            val entrance: String?,
            val exit: String?
    )

    data class DelPresent(
            val function: String,
            val id_present: String?
    )

}