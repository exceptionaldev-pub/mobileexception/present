package ir.execptionaldev.presentapp.repository.local

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import ir.execptionaldev.presentapp.repository.models.Get.User

@androidx.room.Database(entities = [User::class], version = 9, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "present.mDb")
                .fallbackToDestructiveMigration()
                .build()
    }
}