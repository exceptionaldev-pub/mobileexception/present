package ir.execptionaldev.presentapp.repository.remote

import io.reactivex.Single
import ir.execptionaldev.presentapp.repository.models.Get.Status
import ir.execptionaldev.presentapp.repository.models.Get.UserList
import ir.execptionaldev.presentapp.repository.models.Send.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface APIService {
    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun login(@Body loginData: LoginData): Single<UserList>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun register(@Body registerData: RegisterData): Single<Status>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun getPresentList(@Body presentListData: PresentListData): Single<UserList>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun getPresentHistory(@Body presentHistoryData: PresentHistoryData): Single<UserList>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun addTime(@Body addTimeData: AddTimeData): Single<Status>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun getUsers(@Body getUsersData: GetUsersData): Single<UserList>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun editProf(@Body editProf: EditProf): Single<Status>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun addAbsent(@Body addTimeData: AddTimeData): Single<Status>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun getAbsent(@Body presentHistoryData: PresentHistoryData): Single<UserList>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun addPayment(@Body addPayment: AddPayment): Single<Status>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun getPayment(@Body getUsersData: GetUsersData): Single<UserList>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun editPresent(@Body editPresent: EditPresent): Single<Status>

    @POST("controller/index.php")
    @Headers("User-Agent: present-app")
    fun delPresent(@Body delPresent: DelPresent): Single<Status>


    companion object {
        val instance: APIService by lazy {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://URL")
                    .build()

            retrofit.create(APIService::class.java)
        }
    }
}

