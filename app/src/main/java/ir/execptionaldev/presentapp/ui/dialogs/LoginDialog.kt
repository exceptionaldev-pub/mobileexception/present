package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.ui.activities.MainActivity
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_login.*
import org.jetbrains.anko.startActivity

class LoginDialog(private val mActivity: AppCompatActivity) : Dialog(mActivity), View.OnClickListener {
    private val mModel by lazy {
        mActivity.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        }
    }

    //TODO: disposable
    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_login)

        btn_login.setOnClickListener(this)
    }

    @SuppressLint("CheckResult")
    override fun onClick(v: View?) {
        mModel.login(til_user.editText?.text.toString(), til_pass.editText?.text.toString())
                .subscribe({ result ->
                    mModel.mDb.userDao().insert(result.user_list[0])
                    dismiss()
                    mActivity.finish()
                    mActivity.startActivity<MainActivity>()
                }, { it.printStackTrace() })
        dismiss()
    }
}