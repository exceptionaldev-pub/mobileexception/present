package ir.execptionaldev.presentapp.ui.activities

//import com.orhanobut.dialogplus.DialogPlus
//import com.orhanobut.dialogplus.ViewHolder
import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.leinardi.android.speeddial.SpeedDialView
import io.reactivex.disposables.CompositeDisposable
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.ui.dialogs.AddTimeDialog
import ir.execptionaldev.presentapp.ui.dialogs.EditProfDialog
import ir.execptionaldev.presentapp.ui.dialogs.LoginDialog
import ir.execptionaldev.presentapp.ui.dialogs.OtherDayDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.startActivity
import java.util.*


class MainActivity : AppCompatActivity(), OnClickListener, SwipeRefreshLayout.OnRefreshListener, SpeedDialView.OnActionSelectedListener {

    //    private val mDialogPlus by lazy { setupDialogPlus() }
    private val mModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        compositeDisposable.add(mModel.getUser().subscribe({ user ->
            if (user.id_user > 9)
                finish()
            refresh()
            if (user.rank == "Admin") {
//                mDialogPlus.holderView.run {
//                    io_rank.visibility = INVISIBLE
//                    exp_rank.visibility = VISIBLE
//                }
                speedDial.apply {
                    addAllActionItems(
                            listOf(
//                            SpeedDialActionItem.Builder(R.id.fab, android.R.drawable.ic_menu_edit)
//                                    .setLabel(getString(R.string.EditProf))
//                                    .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
//                                    .setLabelBackgroundColor(ResourcesCompat.getColor(resources, android.R.color.transparent, theme))
//                                    .setLabelColor(ResourcesCompat.getColor(resources, R.color.white, theme))
//                                    .create(),
                                    SpeedDialActionItem.Builder(R.id.action_bar, android.R.drawable.ic_menu_close_clear_cancel)
//                                            .setLabel(getString(R.string.ExitApp))
                                            .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
                                            .setLabelBackgroundColor(ResourcesCompat.getColor(resources, android.R.color.transparent, theme))
                                            .setLabelColor(ResourcesCompat.getColor(resources, R.color.white, theme))
                                            .create(),
                                    SpeedDialActionItem.Builder(R.id.accelerateDecelerate, android.R.drawable.ic_menu_edit)
//                                            .setLabel(getString(R.string.EditProf))
                                            .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
                                            .setLabelBackgroundColor(ResourcesCompat.getColor(resources, android.R.color.transparent, theme))
                                            .setLabelColor(ResourcesCompat.getColor(resources, R.color.white, theme))
                                            .create(),
                                    SpeedDialActionItem.Builder(R.id.accelerate, android.R.drawable.ic_dialog_dialer)
//                                            .setLabel(getString(R.string.OtherDay))
                                            .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
                                            .setLabelBackgroundColor(ResourcesCompat.getColor(resources, android.R.color.transparent, theme))
                                            .setLabelColor(ResourcesCompat.getColor(resources, R.color.white, theme))
                                            .create()
                            )
                    )
                    setOnActionSelectedListener(this@MainActivity)
                }
            } else {
//                mDialogPlus.holderView.run {
//                    exp_rank.visibility = INVISIBLE
//                    io_rank.visibility = VISIBLE
//                }
                speedDial.apply {
                    addAllActionItems(
                            listOf(
                                    SpeedDialActionItem.Builder(R.id.action_bar, android.R.drawable.ic_menu_close_clear_cancel)
//                                            .setLabel(getString(R.string.ExitApp))
                                            .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
                                            .setLabelBackgroundColor(ResourcesCompat.getColor(resources, android.R.color.transparent, theme))
                                            .setLabelColor(ResourcesCompat.getColor(resources, R.color.white, theme))
                                            .create(),
                                    SpeedDialActionItem.Builder(R.id.accelerateDecelerate, android.R.drawable.ic_menu_edit)
//                                            .setLabel(getString(R.string.EditProf))
                                            .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme))
                                            .setLabelBackgroundColor(ResourcesCompat.getColor(resources, android.R.color.transparent, theme))
                                            .setLabelColor(ResourcesCompat.getColor(resources, R.color.white, theme))
                                            .create()
                            )
                    )
                    setOnActionSelectedListener(this@MainActivity)
                }
            }
            btn_login_app.visibility = GONE
            rv_present.visibility = VISIBLE
            swipeContainer.visibility = VISIBLE
            btn_add_time.visibility = VISIBLE
            speedDial.visibility = VISIBLE
        }, { it.printStackTrace() }))

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        rv_present.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            adapter = mModel.mAdapter
        }

        toolbar_main.setOnClickListener(this)

        swipeContainer.setOnRefreshListener(this)

        btn_login_app.setOnClickListener(this)

        btn_add_time.setOnClickListener(this)
    }

//    private fun setupDialogPlus(): DialogPlus =
//            DialogPlus.newDialog(this)
//                    .setExpanded(true, 200)
//                    .setGravity(TOP)
//                    .setContentHolder(ViewHolder(R.layout.dialog_menu))
//                    .create().apply {
//                        holderView.run {
//                            present_list.setOnClickListener {
//                                dismiss()
//                                mModel.loadPresentList()
//                            }
//
//                            my_time.setOnClickListener {
//                                dismiss()
//                                longToast("این قسمت تا اطلاع ثانوی فعال نمیباشد :(")
//                            }
//
//                            show_absent.setOnClickListener {
//                                dismiss()
//                                mModel.loadAbsentList().subscribe({ result ->
//                                    mModel.mAdapter.updateItems(result.user_list)
//                                }, { throwable -> throwable.printStackTrace() })
//                            }
//
//                            show_payments.setOnClickListener {
//                                dismiss()
//                                mModel.loadPaymentList().subscribe({ result ->
//                                    mModel.mAdapter.updateItems(result.user_list)
//                                }, { throwable -> throwable.printStackTrace() })
//                            }
//                        }
//                    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_login_app -> LoginDialog(this).show()
            R.id.btn_add_time -> AddTimeDialog(this).show()
//            R.id.toolbar_main -> mDialogPlus.show()
        }
    }

    override fun onRefresh() {
        refresh()
    }

    private fun refresh() =
            mModel.loadPresentHistoryList(getCurrentDate()).subscribe({ result ->
                mModel.mAdapter.updateItems(result.user_list)
                swipeContainer.isRefreshing = false
            }, {
                it.printStackTrace()
                swipeContainer.isRefreshing = false
            })

    override fun onActionSelected(actionItem: SpeedDialActionItem?): Boolean =
            when (actionItem?.id) {
//                R.id.fab -> {
//                    AddTimeDialog(this).show()
//                    false // true to keep the Speed Dial open
//                }
                R.id.accelerate -> {
                    OtherDayDialog(this).show()
                    false // true to keep the Speed Dial open
                }
                R.id.action_bar -> {
                    mModel.delUser().subscribe {
                        finish()
                        startActivity<MainActivity>()
                    }
                    false // true to keep the Speed Dial open
                }
                R.id.accelerateDecelerate -> {
                    EditProfDialog(this).show()
                    false // true to keep the Speed Dial open
                }
                else -> false
            }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    fun getCurrentDate() =
            Calendar.getInstance().run {
                "${get(Calendar.YEAR)}-${addZero(get(Calendar.MONTH) + 1)}-${addZero(get(Calendar.DAY_OF_MONTH))}"
            }

    fun addZero(i: Int) = if (i < 10) "0$i" else "$i"

}
