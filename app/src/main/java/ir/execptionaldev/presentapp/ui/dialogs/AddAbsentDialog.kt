package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.repository.models.Get.User
import ir.execptionaldev.presentapp.repository.models.Send.AddTimeData
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_absent.*
import org.jetbrains.anko.longToast

class AddAbsentDialog(private val mActivity: AppCompatActivity) : Dialog(mActivity) {
    private val mModel by lazy {
        mActivity.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        }
    }
    private var id = ""
    private var date = ""
    private var descr = ""

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.dialog_absent)
        uid.adapter = mModel.mSprAdapter

        btn_confirm.setOnClickListener {
            initializeData()
            mModel.addAbsent(AddTimeData("absent",
                    id, date, null, descr))
                    .subscribe({
                        //TODO: use status?
                        mActivity.longToast(" با موفقیت ذخیره شد :)")
                    }, { throwable -> throwable.printStackTrace() })
            dismiss()
        }
    }

    private fun initializeData() {
        id = (uid.selectedItem as User).id_user.toString()
        date = if (cdate.text.toString() == "")
            mModel.getCurrentDate()
        else
            cdate.text.toString()
        descr = if (desc.text.toString() == "")
            "-"
        else
            desc.text.toString()
    }

}