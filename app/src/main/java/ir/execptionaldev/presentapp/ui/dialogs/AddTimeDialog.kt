package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.repository.models.Get.User
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_add_time.*
import org.jetbrains.anko.longToast
import java.util.*

class AddTimeDialog(private val mActivity: AppCompatActivity) : Dialog(mActivity), View.OnClickListener {
    private val mModel by lazy {
        mActivity.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        }
    }
    private var id = ""
    private var descr = ""

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mModel.getUser().subscribe({ user ->
            if (user.rank != "Admin") {
                exception_add.visibility = View.GONE
            }
        }, { it.printStackTrace() })

        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.dialog_add_time)
        uid.adapter = mModel.mSprAdapter

        tv_date.text = mModel.getCurrentDate()
        tv_time.text = mModel.getCurrentTime()

        btn_add_entrance.setOnClickListener(this)

        btn_add_exit.setOnClickListener(this)

        tv_date.setOnClickListener(this)

        tv_time.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_add_entrance -> {
                checkFields()
                addTime("entrance_time")
            }
            R.id.btn_add_exit -> {
                checkFields()
                addTime("exit_time")
            }
            R.id.tv_date -> {
                val c = Calendar.getInstance()
                DatePickerDialog(mActivity, { _, year, monthOfYear, dayOfMonth ->
                    tv_date.text = "$year-${mModel.addZero(monthOfYear) + 1}-${mModel.addZero(dayOfMonth)}"
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show()
            }
            R.id.tv_time -> {
                val c = Calendar.getInstance()
                TimePickerDialog(mActivity, { _, hourOfDay, minute ->
                    tv_time.text = "${mModel.addZero(hourOfDay)}:${mModel.addZero(minute)}:${mModel.addZero(Calendar.getInstance().get(Calendar.SECOND))}"
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show()
            }
        }
    }

    private fun checkFields() {
        descr = if (desc.text.toString() == "")
            "-"
        else
            desc.text.toString()
    }


    private fun addTime(function: String) =
            mModel.getUser().subscribe({ user ->
                id = if (user.rank != "Admin")
                    user.id_user.toString()
                else
                    (uid.selectedItem as User).id_user.toString()
                mModel.addTime(id, tv_date.text.toString(), tv_time.text.toString(), descr, function)
                        .subscribe({
                            //TODO: use status?
                            mActivity.longToast("با موفقیت ذخیره شد :)")
                        }, { it.printStackTrace() })
                dismiss()
            }, { it.printStackTrace() })

}