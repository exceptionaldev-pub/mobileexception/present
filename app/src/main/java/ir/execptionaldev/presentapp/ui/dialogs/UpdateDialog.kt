package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_update.*
import org.jetbrains.anko.longToast
import java.util.*

class UpdateDialog(private val mActivity: Context, private val mainViewModel: MainViewModel, val name: String, val idPresent: String?, val entrance: String?, val exit: String?) : Dialog(mActivity), View.OnClickListener {

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.dialog_update)

        tv_name.text = name
        tv_enter.text = entrance
        tv_exit.text = exit

        btn_update.setOnClickListener(this)
        btn_delete.setOnClickListener(this)

        tv_enter.setOnClickListener(this)
        tv_exit.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_update -> {
                mainViewModel.updatePresent(idPresent, tv_enter.text.toString(), tv_exit.text.toString())
                        .subscribe({ _ ->
                            mActivity.longToast("با موفقیت انجام شد :)")
                            dismiss()
                        }, { it.printStackTrace() })
            }
            R.id.btn_delete -> {
                mainViewModel.delPresent(idPresent)
                        .subscribe({ _ ->
                            mActivity.longToast("با موفقیت انجام شد :)")
                            dismiss()
                        }, { it.printStackTrace() })
            }
            R.id.tv_enter -> {
                val c = Calendar.getInstance()
                TimePickerDialog(mActivity, { _, hourOfDay, minute ->
                    tv_enter.text = "${addZero(hourOfDay)}:${addZero(minute)}:${addZero(Calendar.getInstance().get(Calendar.SECOND))}"
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show()
            }
            R.id.tv_exit -> {
                val c = Calendar.getInstance()
                TimePickerDialog(mActivity, { _, hourOfDay, minute ->
                    tv_exit.text = "${addZero(hourOfDay)}:${addZero(minute)}:${addZero(Calendar.getInstance().get(Calendar.SECOND))}"
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show()
            }
        }
    }
    fun addZero(i: Int) = if (i < 10) "0$i" else "$i"
}