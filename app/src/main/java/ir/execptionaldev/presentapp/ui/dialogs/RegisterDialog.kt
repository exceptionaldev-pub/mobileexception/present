package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_register.*
import org.jetbrains.anko.longToast

class RegisterDialog(private val mActivity: AppCompatActivity) : Dialog(mActivity), View.OnClickListener {
    private val mModel by lazy {
        mActivity.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_register)

        //...

        btn_register.setOnClickListener(this)
    }

    @SuppressLint("CheckResult")
    override fun onClick(v: View?) {
        mModel.register(et_fname.text.toString(), et_lname.text.toString(), et_uname.text.toString(), et_pass.text.toString())
                .subscribe({ result ->
                    val status = result.status[0]
                    mActivity.longToast(status.code + status.message)
                }, { it.printStackTrace() })
        dismiss()
    }


}
