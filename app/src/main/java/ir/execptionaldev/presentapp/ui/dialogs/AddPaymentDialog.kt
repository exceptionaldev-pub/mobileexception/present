package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.repository.models.Get.User
import ir.execptionaldev.presentapp.repository.models.Send.AddPayment
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_add_payment.*
import org.jetbrains.anko.longToast


class AddPaymentDialog(private val mActivity: AppCompatActivity) : Dialog(mActivity), View.OnClickListener {
    private val mModel by lazy {
        mActivity.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        }
    }
    private var id = ""
    private var money = 0F
    private var descr = ""


    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.dialog_add_payment)
        uid.adapter = mModel.mSprAdapter

        btn_confirm.setOnClickListener(this)
    }

    @SuppressLint("CheckResult")
    override fun onClick(v: View?) {
        initializeData()
        mModel.addPayment(AddPayment("payments_history",
                id, money, descr, mModel.getCurrentDate()))
                .subscribe({
                    //TODO: use status?
                    mActivity.longToast(" با موفقیت ذخیره شد :)")
                }, { throwable -> throwable.printStackTrace() })
        dismiss()
    }

    private fun initializeData() {
        id = (uid.selectedItem as User).id_user.toString()
        money = cmoney.text.toString().toFloat()
        descr = if (desc.text.toString() == "")
            "-"
        else
            desc.text.toString()
    }

}