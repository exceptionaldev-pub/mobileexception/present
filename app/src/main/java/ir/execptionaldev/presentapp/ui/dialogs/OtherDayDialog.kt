package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.repository.models.Get.User
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_add_time.*
import kotlinx.android.synthetic.main.dialog_other_day.*
import kotlinx.android.synthetic.main.dialog_other_day.tv_date
import org.jetbrains.anko.longToast
import java.util.*

class OtherDayDialog(private val mActivity: AppCompatActivity) : Dialog(mActivity), View.OnClickListener {
    private val mModel by lazy {
        mActivity.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        }
    }
    private var id = ""

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.dialog_other_day)

        tv_date.text = mModel.getCurrentDate()

        tv_date.setOnClickListener(this)
        btn_confirm.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_date -> {
                val c = Calendar.getInstance()
                DatePickerDialog(mActivity, { _, year, monthOfYear, dayOfMonth ->
                    tv_date.text = "$year-${mModel.addZero(monthOfYear) + 1}-${mModel.addZero(dayOfMonth)}"
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show()
            }
            R.id.btn_confirm -> {
                dismiss()
                mModel.loadAllPresent(tv_date.text.toString()).subscribe({ result ->
                    mModel.mAdapter.updateItems(result.user_list)
                }, {
                    it.printStackTrace()
                })
            }
        }
    }



}