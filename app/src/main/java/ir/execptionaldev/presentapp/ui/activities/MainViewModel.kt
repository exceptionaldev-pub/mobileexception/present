package ir.execptionaldev.presentapp.ui.activities

import android.app.Application
import android.widget.ArrayAdapter
import androidx.lifecycle.AndroidViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.repository.local.AppDatabase
import ir.execptionaldev.presentapp.repository.models.Get.User
import ir.execptionaldev.presentapp.repository.models.Send
import ir.execptionaldev.presentapp.repository.models.Send.*
import ir.execptionaldev.presentapp.repository.remote.APIService
import ir.execptionaldev.presentapp.ui.adapter.PresentListRVAdapter
import java.util.*

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val mApiService by lazy { APIService.instance }
    val mDb by lazy { AppDatabase.getInstance(application.applicationContext) }
    val mAdapter by lazy { PresentListRVAdapter(this) }
    val mSprAdapter: ArrayAdapter<User>

    init {
        mSprAdapter = ArrayAdapter<User>(application.applicationContext, R.layout.simple_spinner_item)
                .apply {
                    setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                    getUsers().subscribe({ result ->
                        if (!isEmpty)
                            clear()
                        this.addAll(result.user_list)
                    }, { it.printStackTrace() })
                }
    }

    fun loadPresentList() =
            mApiService.getPresentList(PresentListData("present_list", day(), getCurrentTime()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ result ->
                        mAdapter.updateItems(result.user_list)
                    }, { it.printStackTrace() })

    fun loadAbsentList() =
            mApiService.getAbsent(PresentHistoryData("all_absent", getCurrentDate()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun loadPaymentList() =
            mApiService.getPayment(GetUsersData("get_payments"))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun loadPresentHistoryList(date: String) =
            mApiService.getPresentHistory(PresentHistoryData("present_history_list", date))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun loadAllPresent(date: String) =
            mApiService.getPresentHistory(PresentHistoryData("all_present", date))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun getUser() =
            mDb.userDao().first
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())

    fun delUser() =
            mDb.userDao().deleteAll()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())


    fun getUsers() =
            mApiService.getUsers(GetUsersData("get_all_users"))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun addTime(id: String, date: String, time: String, descr: String, function: String) =
            mApiService.addTime(AddTimeData(function, id, date, time, descr))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun register(name: String, lname: String, uname: String, pass: String) =
            mApiService.register(RegisterData("register",
                    name, lname, uname, pass, "IO"))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun login(user: String, pass: String) =
            mApiService.login(LoginData("login",
                    user, pass))
                    .subscribeOn(Schedulers.io())

    fun editProfile(id: String, name: String, lname: String, uname: String, pass: String, email: String) =
            mApiService.editProf(EditProf("edit_profile",
                    id, name, lname, uname, pass, email))
                    .subscribeOn(Schedulers.io())

    fun updateUser(user: User, name: String, lname: String, uname: String, email: String) =
            mDb.userDao().update(User(user.id_user,
                    name, lname, uname, email,
                    null, null, null, null, null))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun addAbsent(addTimeData: Send.AddTimeData) =
            mApiService.addAbsent(addTimeData)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun addPayment(addPayment: AddPayment) =
            mApiService.addPayment(addPayment)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun updatePresent(idPresent: String?, entrance: String?, exit: String?) =
            mApiService.editPresent(EditPresent("edit_present", idPresent, entrance, exit))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun delPresent(idPresent: String?) =
            mApiService.delPresent(DelPresent("delete_present", idPresent))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    private fun day() =
            when (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
                1 -> "Sun"
                2 -> "Mon"
                3 -> "Tue"
                4 -> "Wed"
                5 -> "Thu"
                6 -> "Fri"
                7 -> "Sat"
                else -> "Nope!"
            }

    fun getCurrentTime() =
            Calendar.getInstance().run {
                "${addZero(get(Calendar.HOUR_OF_DAY))}:${addZero(get(Calendar.MINUTE))}:${addZero(get(Calendar.SECOND))}"
            }


    fun getCurrentDate() =
            Calendar.getInstance().run {
                "${get(Calendar.YEAR)}-${addZero(get(Calendar.MONTH) + 1)}-${addZero(get(Calendar.DAY_OF_MONTH))}"
            }


    fun addZero(i: Int) = if (i < 10) "0$i" else "$i"
}