package ir.execptionaldev.presentapp.ui.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import kotlinx.android.synthetic.main.dialog_edit_prof.*
import org.jetbrains.anko.longToast

class EditProfDialog(private val mActivity: AppCompatActivity) : Dialog(mActivity) {
    private val mModel by lazy {
        mActivity.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        }
    }

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mModel.getUser().subscribe({ user ->
            et_fname.setText(user.firstname)
            et_lname.setText(user.lastname)
            et_uname.setText(user.username)
            et_email.setText(user.email)

            btn_edit.setOnClickListener {
                tv_error.visibility = GONE
                if (et_pass.text.toString() == "" || et_repass.text.toString() == "") {
                    tv_error.text = "لطفا پسورد جدید را وارد کنید"
                    tv_error.visibility = VISIBLE
                }
                else if (et_pass.text.toString() != et_repass.text.toString()) {
                    tv_error.text = "پسورد و تکرار آن یکسان نیستند"
                    tv_error.visibility = VISIBLE
                }
                else if (et_email.text.toString() == "") {
                    tv_error.text = "لطفا ایمیل خود را وارد کنید"
                    tv_error.visibility = VISIBLE
                }
                else {
                    mModel.editProfile(user.id_user.toString(),
                            et_fname.text.toString(),
                            et_lname.text.toString(),
                            et_uname.text.toString(),
                            et_pass.text.toString(),
                            et_email.text.toString()).subscribe({
                        //TODO: use status?
                        mModel.updateUser(user, et_fname.text.toString(),
                                et_lname.text.toString(),
                                et_uname.text.toString(),
                                et_email.text.toString())
                                .subscribe({ mActivity.longToast("اطلاعات با موفقیت ثبت شد!") }, {})
                    }, { throwable -> throwable.printStackTrace() })
                    dismiss()
                }
            }
        }, { it.printStackTrace() })

        setContentView(R.layout.dialog_edit_prof)

    }

}
