package ir.execptionaldev.presentapp.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.execptionaldev.presentapp.R
import ir.execptionaldev.presentapp.repository.models.Get.User
import ir.execptionaldev.presentapp.ui.activities.MainViewModel
import ir.execptionaldev.presentapp.ui.dialogs.UpdateDialog
import kotlinx.android.synthetic.main.cardview_present_history.view.*
import java.util.*

class PresentListRVAdapter(private val mainViewModel: MainViewModel) : RecyclerView.Adapter<PresentListRVAdapter.PresentViewHolder>() {
    private var mList = ArrayList<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PresentViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.cardview_present_history, parent, false)
        return PresentViewHolder(v)
    }

    override fun onBindViewHolder(holder: PresentViewHolder, position: Int) {
        holder.bindList(mList[position], mainViewModel)
    }

    class PresentViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {

        override fun onClick(p0: View?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        @SuppressLint("SetTextI18n")
        fun bindList(user: User, mainViewModel: MainViewModel) {
            itemView.run {
                rl_enter.visibility = VISIBLE
                rl_exit.visibility = VISIBLE

                last_enter.text = ""
                last_exit.text = ""
                name.text = user.firstname + " " + user.lastname

                if (user.entrance_time == null) {
                    rl_enter.visibility = GONE
                    rl_exit.visibility = GONE
                }
                last_enter.text = user.entrance_time
                last_exit.text = user.exit_time

                if (user.balance != null) {
                    rl_balance.visibility = VISIBLE
                    balance.text = user.balance.toString()
                }
            }
            itemView.setOnClickListener {
                mainViewModel.getUser().subscribe { me ->
                    if (me.rank == "Admin")
                        UpdateDialog(mActivity = itemView.context,
                                mainViewModel = mainViewModel,
                                name = user.firstname + " " + user.lastname,
                                entrance = user.entrance_time,
                                exit = user.exit_time,
                                idPresent = user.id_present).show()
                }
            }
        }
    }

    override fun getItemCount() = mList.size

    fun insert(position: Int, user: User) {
        mList.add(position, user)
        notifyItemInserted(position)
    }

    fun remove(user: User) {
        val position = mList.indexOf(user)
        mList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun add(position: Int, user: User) {
        mList.add(position, user)
    }

    fun add(user: User) {
        mList.add(user)
    }

    fun updateItems(items: ArrayList<User>) {
        mList = items
        notifyDataSetChanged()
    }

    fun clearList() {
        mList.clear()
        notifyDataSetChanged()
    }
}


