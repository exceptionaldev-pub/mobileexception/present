plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(29)
    defaultConfig {
        applicationId = "ir.execptionaldev.presentapp"
        minSdkVersion(17)
        targetSdkVersion(29)
        versionCode = 1030
        versionName = "1.0.3"
    }
    buildTypes {
        getByName("debug") {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "d"
            resValue("string", "app_name", "ExpPresent-D")
        }
        getByName("release") {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
            resValue("string", "app_name", "ExpPresent")
        }
        productFlavors {
        }
        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }
    }
}



dependencies {
    implementation(Libs.kotlin_stdlib_jdk8)
    implementation(Libs.retrofit)
    implementation(Libs.adapter_rxjava2)
    implementation(Libs.converter_gson)
    implementation(Libs.rxandroid)
    implementation(Libs.room_runtime)
    kapt(Libs.room_compiler)
    implementation(Libs.room_rxjava2)
    implementation(Libs.anko_commons)
    implementation(Libs.lifecycle_extensions)
    implementation(Libs.lifecycle_common_java8)

    implementation(Libs.appcompat)
    implementation(Libs.material)
    implementation(Libs.recyclerview)
    implementation(Libs.cardview)
//    implementation(Libs.dialogplus)
    implementation(Libs.expandable_layout)
    implementation(Libs.speed_dial)
}
