# Present App

You can track your employee for entering/exiting to/from office with this app.

This app has two privilege access: Admin and User

* **Admin access**: 

    * Add enter/exit for others

    * Update/Delete the present list

    * See present list from last days

    * Edit his/her information

* **User access**:

    * Add enter/exit just for himself/herself

    * Edit his/her information

    * See today present list

## Screenshots

<img src="pic1.jpg" width="400" height="800"/> <img src="pic2.jpg" width="400" height="800"/>
<img src="pic3.jpg" width="400" height="800"/> <img src="pic4.jpg" width="400" height="800"/>
<img src="pic5.jpg" width="400" height="800"/> <img src="pic6.jpg" width="400" height="800"/>



